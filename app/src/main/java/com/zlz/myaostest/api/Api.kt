package com.zlz.myaostest.api

import com.zlz.myaostest.entity.Role
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {
    //https://gitlab.com/CodeMonkeyLeon/testdatarepositories/-/raw/main/FlamePillar.json
    @GET("/{owner}/testdatarepositories/-/raw/main/FlamePillar.json")
    fun getRoles(
        @Path("owner") owner: String
    ): Observable<Role>


    @GET("/{owner}/testdatarepositories/-/raw/main/FlamePillar.json")
    fun getCancelRoles(
        @Path("owner") owner: String
    ): Call<Role>


    @GET("/{owner}/testdatarepositories/-/raw/main/FlamePillar.json")
    fun getSuspend1Roles(
        @Path("owner") owner: String
    ): Call<Role>


    @GET("/{owner}/testdatarepositories/-/raw/main/FlamePillar.json")
    suspend fun getSuspend2Roles(
        @Path("owner") owner: String
    ): Role
}