package com.zlz.myaostest.utils

import android.view.View
import android.view.View.OnAttachStateChangeListener
import kotlinx.coroutines.Job

class AutoDisposableJob(
    private val view: View,
    private val job: Job
) : Job by job, OnAttachStateChangeListener {

    init {
        if (view.isAttachedToWindow) {
            view.addOnAttachStateChangeListener(this)
        } else {
            cancel()
        }
        invokeOnCompletion {
            view.removeOnAttachStateChangeListener(this)
        }
    }

    override fun onViewAttachedToWindow(v: View) = Unit

    override fun onViewDetachedFromWindow(v: View) {
        cancel()
        view.removeOnAttachStateChangeListener(this)
    }
}