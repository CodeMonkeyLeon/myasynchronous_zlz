package com.zlz.myaostest

import android.util.Log
import android.view.View
import android.widget.TextView
import com.uber.autodispose.AutoDispose
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider
import com.zlz.myaostest.api.Api
import com.zlz.myaostest.base.BaseAbstractActivity
import com.zlz.myaostest.entity.Role
import com.zlz.myaostest.utils.AutoDisposableJob
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class MainActivity : BaseAbstractActivity(), View.OnClickListener {
    override fun getLayout() = R.layout.act_main

    companion object {
        const val TAG = "TAG_ZLZ"
        const val BASE_URL = "https://gitlab.com"
    }


    private lateinit var mBtnGet: TextView
    private lateinit var mTvResult: TextView


    private val mDisposables by lazy {
        ArrayList<Disposable>()
    }

    private val mJob by lazy {
        ArrayList<Job>()
    }

    override fun initView() {
        mBtnGet = findViewById(R.id.id_btn_get_data)
        mTvResult = findViewById(R.id.id_tv_res)

        mBtnGet.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        v?.run {
            when (id) {
                R.id.id_btn_get_data -> {
                    mTvResult.text = "等待结果"
//                    getData1()
//                    getData2()
//                    getData3()
//                    getData4()
//                    getData5()
//                    getData6()
//                    getData7()
//                    getData8()
                    getData9(this as TextView)
                }

                else -> {

                }
            }
        }
    }


    private val mRetrofitApi by lazy {
        //超时时长
        val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build()


        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(Api::class.java)
        api
    }


    /**
     * subscribeOn()
     * subscribeOn() 用于指定Observable的执行线程，它影响的是整个Observable链。
     *当你调用 subscribeOn() 时，它将决定Observable在哪个线程上运行。
     * 通常，在使用网络请求或执行耗时操作时，你可以使用 subscribeOn(Schedulers.io()) 将Observable的执行线程切换到IO线程池，以避免阻塞主线程。
     *
     *
     * observeOn()
     * observeOn() 用于指定Observer接收和处理数据的线程。
     * 当你调用 observeOn() 时，它将决定Observer在哪个线程上接收数据。
     * 通常，在使用Observable发射数据后需要在主线程上更新UI时，你可以使用 observeOn(AndroidSchedulers.mainThread()) 将Observer的接收线程切换到主线程。
     *
     *
     * subscribe()
     * subscribe() 用于订阅Observable，并接收它发射的数据。
     * subscribe() 方法接受一个Observer作为参数，用于处理Observable发射的数据和事件。
     * 在Observer的实现中，你可以通过重写 onNext()、onError() 和 onComplete() 方法来处理Observable发射的数据、错误和完成事件。
     */
//    private fun getData1() {
//        mRetrofitApi.getRoles("CodeMonkeyLeon")
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(object : Observer<Role> {
//                override fun onSubscribe(d: Disposable) {
//                    Log.i(TAG, "====== 观察者订阅了：${d.toString()}   ${d.isDisposed}")
//                }
//
//                override fun onNext(t: Role) {
//                    Log.i(TAG, "====== onNext： ${t.getRoleInfo()}")
//                    mTvResult.text = t.getRoleInfo()
//                }
//
//                override fun onError(e: Throwable) {
//                    Log.i(TAG, "====== onError：${e.message}")
//                    mTvResult.text = e.message
//                }
//
//                override fun onComplete() {
//                    // 当请求完成时的回调
//                    // 在这里执行清理工作等操作
//                    Log.i(TAG, "====== onComplete")
//                }
//            })
//    }
//
//
//    private fun getData2() {
//        mRetrofitApi.getRoles("CodeMonkeyLeon")
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .onErrorReturnItem(Role("error", "error", "error"))
//            .subscribe(
//                { role ->
//                    role.run {
//                        mTvResult.text = getRoleInfo()
//                    }
//                },
//                { error ->
//                    error?.run {
//                        mTvResult.text = message
//                    }
//                }
//            )
//    }
//
//
//    private fun getData3() {
//        //RxJavaPlugins.setErrorHandler参考文章https://www.uoften.com/article/210118.html
//        RxJavaPlugins.setErrorHandler {
//            Log.i(TAG, "RxJava全局异常捕获：${it.message}")
////            Exceptions.throwIfFatal(it) //如果是一个很严重的异常，就让它抛出去吧
//        }
//
//
//        mRetrofitApi.getRoles("CodeMonkeyLeon")
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(object : Observer<Role> {
//                override fun onSubscribe(d: Disposable) {
//                    Log.i(TAG, "====== 观察者订阅了：${d.toString()}   ${d.isDisposed}")
//                }
//
//                override fun onNext(t: Role) {
//                    Log.i(TAG, "====== onNext： ${t.getRoleInfo()}")
//                    mTvResult.text = t.getRoleInfo()
//                    throw Exception("Leon Exception")
//                }
//
//                override fun onError(e: Throwable) {
//                    Log.i(TAG, "====== onError：${e.message}")
//                    mTvResult.text = e.message
//                }
//
//                override fun onComplete() {
//                    // 当请求完成时的回调
//                    // 在这里执行清理工作等操作
//                    Log.i(TAG, "====== onComplete")
//                }
//            })
//    }
//
//
//    private fun getData4() {
//
//        //超时时长
//        val okHttpClient = OkHttpClient.Builder()
//            .connectTimeout(60, TimeUnit.SECONDS)
//            .readTimeout(60, TimeUnit.SECONDS)
//            .writeTimeout(60, TimeUnit.SECONDS)
//            .build()
//        val retrofit = Retrofit.Builder()
//            .baseUrl(BASE_URL)
//            .client(okHttpClient)
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//        val api = retrofit.create(Api::class.java)
//
//        val disposable = Observable.create(ObservableOnSubscribe<Role> { emitter ->
//            api.getCancelRoles("CodeMonkeyLeon").enqueue(object : Callback<Role> {
//                override fun onResponse(call: Call<Role>, response: Response<Role>) {
//                    emitter?.run {
//                        if (response.isSuccessful) {
//                            response.body()?.let { role ->
//                                onNext(role)
//                            }
//                        } else {
//                            response.errorBody()?.let {
//                                val error = Exception("接口返回异常：${it.toString()}")
//                                onError(error)
//                            }
//                        }
//                    }
//                }
//
//                override fun onFailure(call: Call<Role>, t: Throwable) {
//                    emitter?.run {
//                        onError(t)
//                    }
//                }
//            })
//        })
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(
//                { res ->
//                    mTvResult.text = res.getRoleInfo()
//                },
//                { error ->
//                    mTvResult.text = error.message
//                }
//            )
//
//        mDisposables.add(disposable)
//    }


    private fun getData5() {
        mRetrofitApi.getRoles("CodeMonkeyLeon")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorReturnItem(Role("error", "error", "error"))
            .`as`(AutoDispose.autoDisposable<Role>(AndroidLifecycleScopeProvider.from(this@MainActivity)))
            .subscribe(
                { role ->
                    role.run {
                        mTvResult.text = getRoleInfo()
                    }
                },
                { error ->
                    error?.run {
                        mTvResult.text = message
                    }
                }
            )
    }


    private fun getData6() {

        GlobalScope.launch {
            try {
                val role = getRole()
                runOnUiThread {
                    mTvResult.text = role.getRoleInfo()
                }
            } catch (e: Exception) {
                runOnUiThread {
                    mTvResult.text = e.message
                }
            }
        }

    }


    private suspend fun getRole() = suspendCoroutine<Role> { continuation ->
        mRetrofitApi.getSuspend1Roles("CodeMonkeyLeon").enqueue(object : Callback<Role> {
            override fun onResponse(call: Call<Role>, response: Response<Role>) {
                if (response.isSuccessful) {
                    response.body()?.let { role ->
                        continuation.resume(role)
                    }
                } else {
                    response.errorBody()?.let { error ->
                        continuation.resumeWithException(Exception("返回响应报错：${error.string()}"))
                    }
                }
            }

            override fun onFailure(call: Call<Role>, t: Throwable) {
                continuation.resumeWithException(t)
            }
        })
    }


    private fun getData7() {

        GlobalScope.launch {
            try {
                val role = mRetrofitApi.getSuspend2Roles("CodeMonkeyLeon")
                runOnUiThread {
                    mTvResult.text = role.getRoleInfo()
                }
            } catch (e: Exception) {
                runOnUiThread {
                    mTvResult.text = e.message
                }
            }
        }

    }


    private fun getData8() {

        val job = GlobalScope.launch {
            try {
                val role = getRoleCancel()
                runOnUiThread {
                    mTvResult.text = role.getRoleInfo()
                }
            } catch (e: Exception) {
                runOnUiThread {
                    mTvResult.text = e.message
                }
            }
        }


        mJob.add(job)
    }


    private suspend fun getRoleCancel() = suspendCancellableCoroutine<Role> { continuation ->
        mRetrofitApi.getSuspend1Roles("CodeMonkeyLeon").enqueue(object : Callback<Role> {
            override fun onResponse(call: Call<Role>, response: Response<Role>) {
                if (response.isSuccessful) {
                    response.body()?.let { role ->
                        continuation.resume(role)
                    }
                } else {
                    response.errorBody()?.let { error ->
                        continuation.resumeWithException(Exception("返回响应报错：${error.string()}"))
                    }
                }
            }

            override fun onFailure(call: Call<Role>, t: Throwable) {
                continuation.resumeWithException(t)
            }
        })
    }


    fun Job.asAutoDisposable(view: View) = AutoDisposableJob(view, this)


    fun getData9(view: TextView) {
        GlobalScope.launch {
            try {
                val role = mRetrofitApi.getSuspend2Roles("CodeMonkeyLeon")
                runOnUiThread {
                    mTvResult.text = role.getRoleInfo()
                }
            } catch (e: Exception) {
                runOnUiThread {
                    mTvResult.text = e.message
                }
            }
        }.asAutoDisposable(view)
    }


    override fun onDestroy() {
        super.onDestroy()
        mDisposables.forEach {
            Log.i(TAG, "取消RxJava")
            it.dispose()
        }

        mJob.forEach {
            it.cancel()
        }
    }

}
