package com.zlz.myaostest.entity

import java.io.Serializable

data class Role(
    private val name: String = "",
    private val sex: String = "",
    private val attributes: String = ""
) : Serializable {


    fun getRoleInfo() = "角色名称：$name\n角色性别：$sex\n角色属性：$attributes"


    fun displayInfo() {
        println("角色名称: $name")
        println("角色性别: $sex")
        println("角色属性: $attributes")
        println("------------------------------------------")
    }
}
