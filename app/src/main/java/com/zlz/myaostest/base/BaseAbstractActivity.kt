package com.zlz.myaostest.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.activity.ComponentActivity
abstract class BaseAbstractActivity : ComponentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getLayout() != 0) {
            configView()
            initView()
        }
    }


    private fun configView() {
        val inflater = LayoutInflater.from(this)
        val baseView = inflater.inflate(getLayout(), null)
        setContentView(baseView)
    }


    protected abstract fun getLayout(): Int

    protected abstract fun initView()

}